<a name="0.5.4"></a>
## [0.5.4](https://github.com/SNCFDevelopers/wcs-sandbox/compare/v0.1.0...v0.5.4) (2019-09-05)


### Bug Fixes

* **button:** Avenir font doesn't work on gitlab pages ([9535295](https://github.com/SNCFDevelopers/wcs-sandbox/commit/9535295))
* [#61](https://github.com/SNCFDevelopers/wcs-sandbox/issues/61) replace deprecated reflectToAttr ([1a1a7d8](https://github.com/SNCFDevelopers/wcs-sandbox/commit/1a1a7d8))
* Add private to pkg.json ([346a4f8](https://github.com/SNCFDevelopers/wcs-sandbox/commit/346a4f8))
* checkbox for ff < 63 ([4e60171](https://github.com/SNCFDevelopers/wcs-sandbox/commit/4e60171))
* **select:** Emit values as a real array ([476fdd0](https://github.com/SNCFDevelopers/wcs-sandbox/commit/476fdd0))
* **tab:** Remove prop slot ([46d6870](https://github.com/SNCFDevelopers/wcs-sandbox/commit/46d6870))


### Features

* **css:** Better css variables handling ([12e9db4](https://github.com/SNCFDevelopers/wcs-sandbox/commit/12e9db4))
* **css:** Css variables ([70580f4](https://github.com/SNCFDevelopers/wcs-sandbox/commit/70580f4))
* **select:** Multiple select ([#38](https://github.com/SNCFDevelopers/wcs-sandbox/issues/38)) ([74aa887](https://github.com/SNCFDevelopers/wcs-sandbox/commit/74aa887))
* **spinner:** first version ([c2d5b1a](https://github.com/SNCFDevelopers/wcs-sandbox/commit/c2d5b1a))
* **tabs:** fix [#54](https://github.com/SNCFDevelopers/wcs-sandbox/issues/54), fix [#37](https://github.com/SNCFDevelopers/wcs-sandbox/issues/37), fix [#36](https://github.com/SNCFDevelopers/wcs-sandbox/issues/36) ([#63](https://github.com/SNCFDevelopers/wcs-sandbox/issues/63)) ([3b21caa](https://github.com/SNCFDevelopers/wcs-sandbox/commit/3b21caa))



<a name="0.0.7"></a>
## 0.0.7 (2019-07-30)



