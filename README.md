# Web Components SNCF 
This project is the web component implementation of the SNCF's design system.  
It is built using stencil.

## Status
This project is still under heavy development therefore it is not yet released.

## Example usage
There are example projects under `example` folder for:
- Angular
- Vue
- React (WIP)

There will be documentation up and running soon, as of now you can simply refer to the README's of the corresponding example.

If you have any question, don't hesitate to fill an issue !

