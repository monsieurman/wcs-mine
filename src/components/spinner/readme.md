# wcs-spinner



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                | Type                    | Default    |
| -------- | --------- | -------------------------------------------------------------------------- | ----------------------- | ---------- |
| `mode`   | `mode`    | Indicates the spinner display mode. Accepted values: `border` or `growing` | `"border" \| "growing"` | `'border'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
