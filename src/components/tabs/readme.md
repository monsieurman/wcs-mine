# wcs-tabs



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description                | Type                           | Default   |
| --------------- | ---------------- | -------------------------- | ------------------------------ | --------- |
| `align`         | `align`          |                            | `"center" \| "end" \| "start"` | `'start'` |
| `selectedIndex` | `selected-index` | Current selected tab index | `number`                       | `0`       |


## Events

| Event           | Description                          | Type                              |
| --------------- | ------------------------------------ | --------------------------------- |
| `wcsTabsChange` | Emitted when the selected tab change | `CustomEvent<WcsTabsChangeEvent>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
